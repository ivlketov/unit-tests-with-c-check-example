all: money.o check_money.o
	gcc money.o check_money.o -lcheck -lsubunit -lm -lpthread -lrt -o check_money_tests.out

money.o: src/money.c src/money.h
	gcc -c src/money.c

check_money.o: tests/check_money.c src/money.h
	gcc -c tests/check_money.c

clean:
	rm -rf *.o *.out